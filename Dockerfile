FROM alpine AS builder
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ARG USER
ARG PASSWORD
ARG BASEURL="https://download.appdynamics.com/download/prox/download-file"
ARG VERSION

ENV APPD_HOME="/opt/appdynamics"

ADD dbagent.sh ${APPD_HOME}/

RUN apk update  
RUN apk upgrade
RUN apk add unzip curl
RUN chmod +x ${APPD_HOME}/dbagent.sh
RUN curl --referer http://www.appdynamics.com -c /tmp/cookies.txt -d "username=${USER}&password=${PASSWORD}" https://login.appdynamics.com/sso/login/
RUN curl -L -o /tmp/dbagent.zip -b /tmp/cookies.txt ${BASEURL}/db-agent/${VERSION}/db-agent-${VERSION}.zip
RUN unzip /tmp/dbagent.zip -d ${APPD_HOME}/dbagent


FROM openjdk:8-jre-slim
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ENV APPD_HOME="/opt/appdynamics" \
    APPDYNAMICS_CONTROLLER_HOST_NAME="" \
    APPDYNAMICS_CONTROLLER_PORT="8090" \
    APPDYNAMICS_AGENT_ACCOUNT_NAME="customer1" \
    APPDYNAMICS_AGENT_ACCOUNT_ACCESS_KEY="" \
    APPDYNAMICS_CONTROLLER_SSL_ENABLED="false" \
    APPDYNAMICS_AGENT_NAME="dbagent-1"

COPY --from=builder ${APPD_HOME} ${APPD_HOME}

CMD [ "/bin/bash", "-c", "${APPD_HOME}/dbagent.sh" ]
