#!/bin/bash

APPD_MEMORY="256m"

if [ -n "${APPDYNAMICS_MEMORY:+1}" ]
then
    APPD_MEMORY=${APPDYNAMICS_MEMORY}
fi

${APPD_HOME}/dbagent/start-dbagent -Xmx${APPD_MEMORY} -Xms${APPD_MEMORY} -Ddbagent.name=${APPDYNAMICS_AGENT_NAME}