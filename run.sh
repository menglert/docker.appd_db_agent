#!/bin/bash

echo -e "\x1B[7mContainer Name\x1B[27m"
read container

echo -e "\x1B[7mImage Name\x1B[27m"
read image

echo -e "\x1B[7mController Host\x1B[27m"
read host

echo -e "\x1B[7mController Port\x1B[27m"
read port

echo -e "\x1B[7mAccount Name\x1B[27m"
read account

echo -e "\x1B[7mAccount Access Key\x1B[27m"
read key

echo -e "\x1B[7mSSL enabled\x1B[27m"
read ssl

docker run -d --name $container \
    -e APPDYNAMICS_CONTROLLER_HOST_NAME="$host" \
    -e APPDYNAMICS_CONTROLLER_PORT="$port" \
    -e APPDYNAMICS_AGENT_ACCOUNT_NAME="$account" \
    -e APPDYNAMICS_AGENT_ACCOUNT_ACCESS_KEY="$key" \
    -e APPDYNAMICS_CONTROLLER_SSL_ENABLED="$ssl" \
    -e APPDYNAMICS_AGENT_NAME="$container" \
    $image
